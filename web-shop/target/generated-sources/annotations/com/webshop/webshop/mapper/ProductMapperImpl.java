package com.webshop.webshop.mapper;

import com.webshop.webshop.dto.ProductDto;
import com.webshop.webshop.model.Product;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 20.0.1 (Oracle Corporation)"
)
@Component
public class ProductMapperImpl implements ProductMapper {

    @Override
    public ProductDto dto(Product product) {
        if ( product == null ) {
            return null;
        }

        ProductDto productDto = new ProductDto();

        productDto.setName( product.getName() );
        productDto.setPrice( product.getPrice() );
        productDto.setFirm( product.getFirm() );
        productDto.setUser( product.getUser() );

        return productDto;
    }

    @Override
    public List<ProductDto> dtos(List<Product> products) {
        if ( products == null ) {
            return null;
        }

        List<ProductDto> list = new ArrayList<ProductDto>( products.size() );
        for ( Product product : products ) {
            list.add( dto( product ) );
        }

        return list;
    }

    @Override
    public void update(Product product, ProductDto productDto) {
        if ( productDto == null ) {
            return;
        }

        product.setName( productDto.getName() );
        product.setPrice( productDto.getPrice() );
        product.setFirm( productDto.getFirm() );
        product.setUser( productDto.getUser() );
    }
}
